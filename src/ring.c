
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ring.h"

Ring* ring_init(Ring* r, size_t max_items) {
  if (r) {
    r->max_items = max_items;
    r->size = r->max_items * sizeof(double);

    r->data = malloc(r->size);

    r->start = 0;
    r->end = 0;
    r->empty = true;
  }

  return r;
}

void ring_destroy(Ring* r) {
  if (r) {
    free(r->data);
  }
}

Ring* ring_append(Ring* r, double* value) {
  if (r) {
    size_t new_end;
    if (r->empty) {
      new_end = r->end;
      r->empty = false;
    } else {
      new_end = r->end + 1;

      // Cycle the ring
      if (r->end == r->max_items - 1) {
        new_end = 0;
      }

      if (new_end == r->start) {
        r->start++;
      }
    }

    printf("LOG: Writing [%ld]:\t %f\n", new_end, *value);
    (r->data)[new_end] = *value;
    r->end = new_end;
  }
  return r;
}

size_t ring_total_items(Ring* r) {
  if (r) {
    if (r->end > r->start) {
      return r->end - r->start + 1;
    } else if (r->end < r->start) {
      return (r->max_items + r->end) - r->start + 1;
    }
  }
  return 0;
}

double* ring_item(Ring* r, size_t index) {
  size_t offset = r->start + index;

  if (offset >= r->max_items) {
    offset -= r->max_items;
  }

  return r->data + offset;
}

void ring_print(Ring* r) {
  if (r) {
    printf("LOG: Printing ring\n");

    size_t items = ring_total_items(r);

    for (size_t i = 0; i < items; i++) {
      double* p_value = ring_item(r, i);

      double value = *p_value;

      printf("Printing item [%ld]:\t%p\t%f\n", i, p_value, value);
    }
  }
}
