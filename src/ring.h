#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef struct Ring {
  size_t max_items; // Maximum number of stored items
  size_t size;      // Size in memory
  double* data;     // Pointer to memory
  size_t start;     // Position of first element
  size_t end;       // Poisition of last element
  bool empty;       // Signals if there are no items
} Ring;

// Initialize the default values of a ring buffer
Ring* ring_init(Ring* r, size_t max_items);

// Safely destroy a ring buffer from memory
void ring_destroy(Ring* r);

// Append an item to the ring buffer
Ring* ring_append(Ring* r, double* value);

// Get the number of stored items in a ring buffer
size_t ring_total_items(Ring* r);

// Return an item from a ring buffer
double* ring_item(Ring* r, size_t index);

// Print a ring buffer
void ring_print(Ring* r);
