#include <stdio.h>
#include <stdlib.h>

#include "ring.h"

int main(int argc, char* argv[]) {
  Ring ring;
  size_t max_items;

  if (argc > 1) {
    printf("LOG: Using CLI for default size\n");
    max_items = atoi(argv[1]);
  } else {
    max_items = 5;
  }

  ring_init(&ring, max_items);

  printf("LOG: Filling ring with random values\n");
  for (size_t i = 0; i < ring.max_items; i++) {
    double value = (double)rand() / (double)RAND_MAX;
    ring_append(&ring, &value);
  }

  ring_print(&ring);

  ring_destroy(&ring);
  return EXIT_SUCCESS;
}
