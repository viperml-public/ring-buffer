{
  stdenv,
  meson,
  ninja,
}:
stdenv.mkDerivation {
  name = "ccc";
  src = ./.;
  nativeBuildInputs = [
    meson
    ninja
  ];
}
