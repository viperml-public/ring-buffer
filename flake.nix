{
  inputs.flake-parts = {
    url = "github:hercules-ci/flake-parts";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = {
    self,
    nixpkgs,
    flake-parts,
  }:
    flake-parts.lib.mkFlake {inherit self;} {
      systems = ["x86_64-linux"];
      perSystem = {
        pkgs,
        self',
        ...
      }: let
        stdenv = pkgs.clangStdenv;
      in {
        packages = {
          default = pkgs.callPackage ./release.nix {};
          static = pkgs.pkgsStatic.callPackage ./release.nix {};
          win64 = pkgs.pkgsCross.mingwW64.callPackage ./release.nix {};
        };
        devShells.default = pkgs.mkShell.override {inherit stdenv;} {
          packages = with pkgs; [
            clang-tools
            gdb
            treefmt
          ];
          inputsFrom = [self'.packages.default];
        };
      };
    };
}
